const mongoose = require('mongoose');
const _ = require('lodash');
var Schema = mongoose.Schema;

var VacationSchema = new Schema({
    startDate: { type: String, required: true },
    endDate: { type: String, required: true },
    requesterId: { type: (Schema.Types.ObjectId), required: true },
    requesterNotes: { type: String, required: true },
    inspectorId: { type: (Schema.Types.ObjectId) },
    inspectorNotes: { type: String },
    status: { type: String, required: true }
});

VacationSchema.methods.toJSON = function () {
    var vacation = this;
    var vacationObject = vacation.toObject();
    output = _.pick(vacationObject, [
        'startDate',
        'endDate',
        'requesterId',
        'requesterNotes',
        'inspectorId',
        'inspectorNotes',
        'status',
    ]);
    output.id = vacationObject._id;
    return output;
}

var VacationModel = mongoose.model('vacation', VacationSchema);

var VacationAggregate= [
    {
        $lookup:
        {
            from: "users",
            localField: "requesterId",
            foreignField: "_id",
            as: "requester_user"
        }
    },
    { $unwind: "$requester_user" },
    {
        $lookup:
        {
            from: "users",
            localField: "inspectorId",
            foreignField: "_id",
            as: "inspector_user"
        }
    },
    {
        $unwind:
            { path: "$inspector_user", preserveNullAndEmptyArrays: true }
    },
    {
        $project: {
            id: "$_id",
            _id: 0,
            status: 1,
            startDate: 1,
            endDate: 1,
            requesterNotes: 1,
            requesterId: 1,
            requesterNameASurname: { $concat: ["$requester_user.name", " ", "$requester_user.surname"] },
            inspectorNotes: 1,
            inspectorId: 1,
            inspectorNameASurname: { $concat: ["$inspector_user.name", " ", "$inspector_user.surname"] },
        }
    }
]


module.exports = { VacationModel,VacationAggregate };