const mongoose = require('mongoose');
const _ = require('lodash');
var Schema = mongoose.Schema;

var DepartmentSchema = new Schema({
    name: { type: String, required: true }
});

DepartmentSchema.methods.toJSON = function () {
    var department = this;
    var departmentObject = department.toObject();
    let output = _.pick(departmentObject, ['name']);
    output.id = departmentObject._id;
    return output;
}

var DepartmentModel = mongoose.model('department', DepartmentSchema);

module.exports = { DepartmentModel };