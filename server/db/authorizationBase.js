const { mongoose } = require('./mongoose');
const { AuthorizationModel } = require('../models/authorization');

var authorizations = [
    {
        "roleType": 0,
        "authorizations": [
            {
                "_id": "CanHandleNoticeBoard",
                "enabled": false,
                "description": "Può creare e modificare gli annunci aziendali",
            },
            {
                "_id": "CanHandleUsers",
                "enabled": false,
                "description": "Può creare, modificare e eliminare gli utenti",
            },
            {
                "_id": "CanViewUsers",
                "enabled": false,
                "description": "Può visualizzare gli utenti",
            },
            {
                "_id": "CanHandleRoles",
                "enabled": false,
                "description": "Può creare, modificare e eliminare i ruoli ed assegnarli agli utenti",
            },
            {
                "_id": "CanHandlePresenceCalendar",
                "enabled": false,
                "description": "Può inserire e validare le ferie e le presenze degli utenti",
            },
            {
                "_id": "CanHandleAreas",
                "enabled": false,
                "description": "Può gestire le aree dell'azienda, le sedi e i dipartimenti",
            }
        ]
    },
    {
        "roleType": 1,
        "authorizations": [
            {
                "_id": "CanEditProfile",
                "enabled": false,
                "description": "Può modificare il proprio profilo",
            },
            {
                "_id": "CanViewOwnCalendar",
                "enabled": false,
                "description": "Può visualizzare il proprio calendario",
            },
            {
                "_id": "CanHandleOwnCalendar",
                "enabled": false,
                "description": "Può richiedere ferie e permessi",
            },
            {
                "_id": "CanViewNoticeBoard",
                "enabled": false,
                "description": "Può visualizzare la bacheca degli annunci aziendali",
            }
        ]
    }
];


AuthorizationModel.insertMany(authorizations).then((doc) => {
    console.log("Inserite le Autorizzazioni");
    console.log("Closing connection to MongoDB");
    mongoose.connection.close()
}).catch((e) => {
    mongoose.connection.close()
    console.log(e);
    console.log("Closing connection to MongoDB");
});
