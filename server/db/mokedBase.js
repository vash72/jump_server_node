const { mongoose } = require('./mongoose');
const jwt = require('jsonwebtoken');

var UserModel = require('./../models/user').UserModel;
var UserRoleModel = require('./../models/userRole').UserRoleModel;
var VacationModel = require('./../models/vacation').VacationModel;

var id_user1 = new mongoose.Types.ObjectId();
var id_user2 = new mongoose.Types.ObjectId();
var id_user3 = new mongoose.Types.ObjectId();

var id_role1 = new mongoose.Types.ObjectId();//ferie
var id_role2 = new mongoose.Types.ObjectId();
var id_role3 = new mongoose.Types.ObjectId();
var id_role4 = new mongoose.Types.ObjectId();


var done_counter = 0;
var secret = "bagaglioAMano";

var User1 = new UserModel({
    _id: id_user1,
    username: "Xzone",
    name: 'Samuele',
    surname: 'Ipsale',
    address: {
        line: 'via Fasulla',
        city: 'Mont Selice',
        postCode: '35043',
        country: 'Italia',
        countrySubdivision: 'PD'
    },
    hireDate: new Date(684280800000).toISOString(),
    birthday: new Date(684280800000).toISOString(),
    mail: 'zaratustra.mastrazzo@infostradanewmail.com',
    phone: '33344488822',
    mobilePhone: '33355599922',
    password: 'psw',
    roles: [id_role1, id_role2, id_role3]
});

var User2 = new UserModel({
    _id: id_user2,
    username:"Shupafemmine",
    name: 'Gianpietro',
    surname: 'Voleterra',
    address: {
        line: 'via Cesello',
        city: 'Padova',
        postCode: '35043',
        country: 'Italia',
        countrySubdivision: 'PD'
    },
    hireDate: new Date(98319600000).toISOString(),
    birthday: new Date(98319600000).toISOString(),
    mail: 'zaratustra@gmail.com',
    phone: '888',
    mobilePhone: '999',
    password: 'psw',
    roles: [id_role1]
});
var User3 = new UserModel({
    _id: id_user3,
    username:"Buon Samaritano",
    name: 'Gino',
    surname: 'Strada',
    address: {
        line: 'viale Ragusano',
        city: 'Masera di Padova',
        postCode: '35043',
        country: 'Italia',
        countrySubdivision: 'PD'
    },
    hireDate: new Date(98319600000).toISOString(),
    birthday: new Date(98319600000).toISOString(),
    mail: 'znn@znn.zb',
    phone: '888',
    mobilePhone: '999',
    password: 'psw',
    roles: [id_role4]
});

var usersVacation = [
    {
        requesterId: id_user1,
        startDate: new Date(1537660800000).toISOString(),
        endDate: new Date(1535068800000).toISOString(),
        requesterNotes: 'Ah boh',
        inspectorId: id_user1,
        inspectorNotes: 'Inspector note Inspector note Inspector note Inspector note Inspector note Inspector note Inspector note Inspector note ',
        status: 'In attesa'
    },
    {
        requesterId: id_user1,
        startDate: new Date(1536105600000).toISOString(),
        endDate: new Date(1536278400000).toISOString(),
        requesterNotes: 'Ho vinto un miglione di euro',
        inspectorId: id_user1,
        inspectorNotes: 'WTF?!?!',
        status: 'In attesa'
    },
    {
        requesterId: id_user1,
        startDate: new Date(1536105600000).toISOString(),
        endDate: new Date(1536278400000).toISOString(),
        requesterNotes: 'Ah boh',
        inspectorId: id_user1,
        inspectorNotes: 'Inspector note',
        status: 'Rifiutata'
    },
    {
        requesterId: id_user1,
        startDate: new Date(1534896000000).toISOString(),
        endDate: new Date(1535068800000).toISOString(),
        requesterNotes: 'Ah boh',
        inspectorId: id_user1,
        inspectorNotes: 'Inspector note',
        status: 'Approvata'
    },
    {
        requesterId: id_user2,
        startDate: new Date(1534896000000).toISOString(),
        endDate: new Date(1535068800000).toISOString(),
        requesterNotes: 'Ah boh',
        inspectorId: id_user1,
        inspectorNotes: 'Inspector note',
        status: 'In attesa',
        userNameASurname: 'Gianpietro Voleterra'
    },
    {
        requesterId: id_user2,
        startDate: new Date(1536105600000).toISOString(),
        endDate: new Date(1536278400000).toISOString(),
        requesterNotes: 'Ah boh',
        inspectorId: id_user1,
        inspectorNotes: 'Inspector note',
        status: 'In attesa',
        userNameASurname: 'Gianpietro Voleterra'
    },
    {
        requesterId: id_user2,
        startDate: new Date(1534896000000).toISOString(),
        endDate: new Date(1535068800000).toISOString(),
        requesterNotes: 'Ah boh',
        inspectorId: id_user1,
        inspectorNotes: 'Inspector note',
        status: 'In attesa',
        userNameASurname: 'Gianpietro Voleterra'
    },
    {
        requesterId: id_user3,
        startDate: new Date(1536105600000).toISOString(),
        endDate: new Date(1536278400000).toISOString(),
        requesterNotes: 'Ah boh',
        inspectorId: id_user1,
        inspectorNotes: 'Inspector note',
        status: 'In attesa'
    }
];

var usersRoles = [
    // {
    //     name: 'Amministratore',
    //     canManageVacationRead: true,
    //     canManageVacationUpdateSelf: true,
    //     canManageVacationUpdateOthers: true,
    //     canManageUsersRolesRead: true,
    //     canManageUsersRoleCreate: true,
    //     canManageUsersRoleUpdate: true,
    //     canManageUsersRoleDelete: true,
    //     canManageUsersRead: true,
    //     canManageUserCreate: true,
    //     canManageUserUpdate: true,
    //     canManageUserDelete: true
    // },
    {
        _id: id_role1,
        roleType: 0,
        name: 'Amministratore Ferie',
        authorizationIds: [
            'canManageVacationRead',
            'canManageVacationUpdateSelf',
            'canManageVacationUpdateOthers'
        ]
    },
    {
        _id: id_role2,
        roleType: 0,
        name: 'Amministratore Utenti',
        authorizationIds: [
            'CanViewUsers',
            'CanHandleUsers',
        ]
    },
    {
        _id: id_role3,
        roleType: 0,
        name: 'Amministratore Ruoli',
        authorizationIds: [
            'CanHandleRoles',
        ]
    },
    {
        _id: id_role4,
        roleType: 1,
        name: 'User'
    }
];

User1.save().then((promise) => {
    console.log("Inserito utente Mock 1");
}).then(() => {
    return User2.save().then(() => {
        console.log("Inserito utente Mock 2");
    }).catch((e) => {
        throw (e);
    });
}).then(() => {
    return User3.save().then(() => {
        console.log("Inserito utente Mock 3");
    }).catch((e) => {
        throw (e);
    });
}).then(() => {
    return VacationModel.insertMany(usersVacation).then((doc) => {
        console.log("Inserite le ferie Mock");
    }).catch((e) => {
        throw (e);
    });
}).then(() => {
    return UserRoleModel.insertMany(usersRoles).then((doc) => {
        console.log("Inseriti i ruoli Mock");
        console.log("Closing connection to MongoDB");
        mongoose.connection.close()
    }).catch((e) => {
        throw (e);
    });
}).catch((e) => {
    mongoose.connection.close()
    console.log(e);
    console.log("Closing connection to MongoDB");
});