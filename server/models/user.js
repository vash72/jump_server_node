const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

var Schema = mongoose.Schema;

var AddressSchema = new Schema({
    line: { type: String, required: true },
    city: { type: String, required: true },
    postCode: { type: Number, required: true },
    countrySubdivision: { type: String, required: true },
    country: { type: String, required: true }
});

var UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    name: { type: String, required: true, },
    surname: { type: String, required: true },
    hireDate: { type: String, required: true },
    birthday: { type: String, required: true },
    address: AddressSchema,
    mail: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: (value => {
                return validator.isEmail(value);
            }),
            message: '{VALUE} is not a valid email'
        }
    },
    phone: { type: String, required: true },
    mobilePhone: { type: String, required: true },

    roles: {
        type: [Schema.Types.ObjectId],
        require: true
    },

    password: {
        type: String
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

UserSchema.pre('save', function (next) {
    var user = this;
    if (user.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            });
        });

    } else {
        next();
    }
});


UserSchema.methods.toJSON = function () {
    var user = this;
    var userObject = user.toObject();
    let output = _.pick(userObject, [
        'username',
        'name',
        'surname',
        'hireDate',
        'birthday',
        'mail',
        'phone',
        'mobilePhone',
        'roles',
        'address',
        'birthday',
    ]);
    output.id = userObject._id;
    output.address = output.address ? _.pick(output.address, ['line', 'city', 'postCode', 'countrySubdivision', 'country']) : undefined;
    return output;
};

UserSchema.methods.generateAuthToken = function () {
    var user = this;
    var access = 'auth';
    var token = jwt.sign({ _id: user._id.toHexString(), access }, "bagaglioAMano").toString();
    user.tokens = user.tokens.concat([{ access, token }]);

    return user.save().then(() => {
        return token;
    }).catch((e) => {
        return e;
    });
};

UserSchema.methods.removeToken = function (my_token) {
    var user = this;
    return user.update({
        $pull: {
            tokens: {
                token: my_token
            }
        }
    })
}

UserSchema.statics.findByToken = function (token) {
    var userModel = this;
    var decoded;

    try {
        decoded = jwt.verify(token, "bagaglioAMano");
    } catch (e) {
        return new Promise((resolve, reject) => {
            reject();
        });
    }

    return userModel.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });
};

UserSchema.statics.findByCredentials = function (username, password) {
    var userModel = this;

    return userModel.findOne({ username }).then((user) => {
        if (!user) {
            return Promise.reject();
        }

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, user.password, (err, res) => {
                if (res) {
                    resolve(user);
                } else {
                    reject()
                }
            });
        });
    });
}

var UserModel = mongoose.model('users', UserSchema);
var UserAggregate = [
    {
        "$lookup": {
            from: "users-roles",
            localField: "roles",
            foreignField: "_id",
            as: "roles"
        }
    },
    { "$unwind": { path: "$roles", preserveNullAndEmptyArrays: true } },
    {
        "$project": {
            id: "$_id",
            roles: {
                name: '$roles.name',
                id: '$roles._id'
            },
            username: 1,
            name: 1,
            surname: 1,
            hireDate: 1,
            birthday: 1,
            mail: 1,
            phone: 1,
            mobilePhone: 1,
            address: 1,
        }
    },
    {
        "$group": {
            _id: "$_id",

            "roles": { "$push": "$roles" },

            username: { $first: '$username' },
            name: { $first: '$name' },
            surname: { $first: '$surname' },
            hireDate: { $first: '$hireDate' },
            birthday: { $first: '$birthday' },
            mail: { $first: '$mail' },
            phone: { $first: '$phone' },
            mobilePhone: { $first: '$mobilePhone' },
            address: { $first: '$address' },
        }
    },
    {
        "$project": {
            id: "$_id",
            roles: 1,
            username: 1,
            name: 1,
            surname: 1,
            hireDate: 1,
            birthday: 1,
            mail: 1,
            phone: 1,
            mobilePhone: 1,
            address: 1,
            _id: 0
        }
    },
    { $sort: { name: 1, surname: 1 } }
]
module.exports = { UserModel, UserAggregate };