const mongoose = require('mongoose');
const _ = require('lodash');
var Schema = mongoose.Schema;

var UserRoleSchema = new Schema({
    name: { type: String, required: true, unique: true },
    roleType: { type: Number, required: true },
    authorizationIds: { type: [String] }
});

UserRoleSchema.methods.toJSON = function () {
    userRole = this;
    let userRoleObject = userRole.toObject();
    let output = _.pick(userRoleObject, [
        "name",
        "roleType",
        "authorizationIds"
    ]);
    output.id = userRoleObject._id;
    return output;
};

var UserRoleModel = mongoose.model('users-roles', UserRoleSchema);
module.exports = { UserRoleModel };