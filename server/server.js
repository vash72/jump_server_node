const express = require('express');
const bodyParser = require('body-parser');
const _ = require('lodash');
const { ObjectID } = require('mongodb');

const { authenticate } = require('./middleware/authenticate');
const { mongoose } = require('./db/mongoose');

//---models
const { UserModel, UserAggregate } = require('./models/user');
const { UserRoleModel } = require('./models/userRole');
const { VacationModel, VacationAggregate } = require('./models/vacation');
const { AuthorizationModel } = require('./models/authorization');
const { DepartmentModel } = require('./models/department');
const { OfficeModel } = require('./models/office');
const { AreaModel } = require('./models/area');


//---models

//--- express
process.env.PORT = 3000;
const app = express();
const port = process.env.PORT;
app.use(bodyParser.json());
//--- express

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization');

    res.setHeader('Access-Control-Expose-Headers', 'Authorization');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});



// simulate delay response //todo rimuovere
// app.use((req, res, next) => {
//     setTimeout(() => next(), 1500);
// });



app.listen(port, () => {
    console.log("Server listening on port " + port);

});

app.get('/api/areas/search', (req, res) => {
    AreaModel.find().populate('office','name -_id').populate('department','name -_id').then((docs) => {
        res.status(200).send(docs);
    }).catch((e) => {
        res.status(404).send(e);
    })
});

app.post('/api/areas', (req, res) => {
    let body = _.pick(req.body, ['name']);
    body.office = req.body.officeId;
    body.department = req.body.departmentId;
    let area = new AreaModel(body);
    area.save().then((doc) => {
        res.status(200).send(doc);
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.get('/api/departments/search', (req, res) => {
    DepartmentModel.find().then((docs) => {
        res.status(200).send(docs);
    }).catch((e) => {
        res.status(404).send(e);
    })
});

app.post('/api/departments', (req, res) => {
    let body = _.pick(req.body, ['name']);
    let department = new DepartmentModel(body);
    department.save().then((doc) => {
        res.status(200).send(doc);
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.get('/api/offices/search', (req, res) => {
    OfficeModel.find().then((docs) => {
        res.status(200).send(docs);
    }).catch((e) => {
        res.status(404).send(e);
    })
});

app.post('/api/offices', (req, res) => {
    let body = _.pick(req.body, ['name']);
    let office = new OfficeModel(body);
    office.save().then((doc) => {
        res.status(200).send(doc);
    }).catch((e) => {
        res.status(400).send(e);
    });
});


//--- /manage -> /users/profiles
app.get('/api/users', authenticate, (req, res) => {//todo mettergli https
    UserModel.aggregate([
        ...UserAggregate
    ]).then((users) => {
        if (!users) {
            return res.status(404).send();
        }

        UserRoleModel.find({}, { _id: 1, name: 1 }).then((roles) => {
            if (!roles) {
                return res.status(404).send();
            }
            res.status(200).send({ users, usersRoles: roles });
        })
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.get('/api/users/search', authenticate, (req, res) => {

    let params = _.pick(req.query, [
        'name',
        'surname',
        'mail',
        // 'phone',
        // 'mobilePhone'
    ]);

    let finder = {};
    if (params.name !== undefined && params.name !== "") finder.name = new RegExp('.*' + params.name + '.*', 'i');
    if (params.surname !== undefined && params.surname !== "") finder.surname = new RegExp('.*' + params.surname + '.*', 'i');
    if (params.mail !== undefined && params.mail !== "") finder.mail = new RegExp('.*' + params.mail + '.*', 'i');
    if (params.phone !== undefined && params.phone !== "") finder.phone = new RegExp('.*' + params.phone + '.*', 'i');
    if (params.mobilePhone !== undefined && params.mobilePhone !== "") finder.mobilePhone = new RegExp('.*' + params.mobilePhone + '.*', 'i');

    UserModel.aggregate([
        { $match: finder },
        ...UserAggregate
    ]).then((users) => {
        if (!users) {
            return res.status(404).send();
        }

        UserRoleModel.find({}, { _id: 1, name: 1 }).then((roles) => {
            if (!roles) {
                return res.status(404).send();
            }
            res.status(200).send({ users, usersRoles: roles });
        })
    }).catch((e) => {
        res.status(400).send(e);
    });
});



app.post('/api/users', authenticate, (req, res) => {//todo https
    let body = _.pick(req.body, ['username', 'name', 'surname', 'hireDate', 'birthday', 'address', 'mail', 'phone', 'mobilePhone', 'roles', 'password'])
    body.address = _.pick(body.address, ['line', 'city', 'postCode', 'countrySubdivision', 'country']);


    let newUser = new UserModel(body);

    newUser.save().then((doc) => {
        if (doc) {
            UserModel.aggregate([
                {
                    $match: { _id: mongoose.Types.ObjectId(doc.id) }
                },
                ...UserAggregate,
            ]).then((aggregateDoc) => {
                if (aggregateDoc) {
                    return res.status(200).send(aggregateDoc[0])
                }
                res.status(404).send();
            }).catch((e) => {
                res.status(404).send();
            })
        }

    }).catch((e) => {
        res.status(404).send(e);
    });
});

app.patch('/api/users', authenticate, (req, res) => {
    let user_id = req.body.id;
    let body = _.pick(req.body, [
        'username',
        'name',
        'surname',
        'hireDate',
        'birthday',
        'address',
        'mail',
        'phone',
        'mobilePhone',
        'roles',
    ]);

    body.address = _.pick(body.address, ['line', 'city', 'postCode', 'countrySubdivision', 'country']);


    if (!ObjectID.isValid(user_id)) {
        return res.status(404).send();//Invalid id
    }

    UserModel.findById(user_id).then((doc) => {
        if (!doc) {
            res.status(404).send();//No Match for this id
        }

        doc.username = body.username ? body.username : doc.username;
        doc.name = body.name ? body.name : doc.name;
        doc.surname = body.surname ? body.surname : doc.surname;
        doc.hireDate = body.hireDate ? body.hireDate : doc.hireDate;
        doc.birthday = body.birthday ? body.birthday : doc.birthday;
        doc.mail = body.mail ? body.mail : doc.mail;
        doc.phone = body.phone ? body.phone : doc.phone;
        doc.mobilePhone = body.mobilePhone ? body.mobilePhone : doc.mobilePhone;
        doc.roles = body.roles ? body.roles : doc.roles;

        doc.address.line = body.address.line ? body.address.line : doc.address.line;
        doc.address.city = body.address.city ? body.address.city : doc.address.city;
        doc.address.cap = body.address.cap ? body.address.cap : doc.address.cap;
        doc.address.countrySubdivision = body.address.countrySubdivision ? body.address.countrySubdivision : doc.address.countrySubdivision;
        doc.address.country = body.address.country ? body.address.country : doc.address.country;


        doc.save().then((doc) => {
            if (doc) {
                UserModel.aggregate([
                    {
                        $match: { _id: mongoose.Types.ObjectId(doc.id) }
                    },
                    ...UserAggregate,
                ]).then((aggregateDoc) => {
                    if (aggregateDoc) {
                        return res.status(200).send(aggregateDoc[0])
                    }
                    res.status(404).send();
                }).catch((e) => {
                    res.status(404).send();
                })
            }

        }).catch((e) => {
            res.status(404).send(e);
        });

    }).catch((e) => {
        res.status(404).send(e);// dunno what happend
    });

})

app.delete('/api/users/:id', authenticate, (req, res) => {
    let user_id = req.params.id;

    if (!ObjectID.isValid(user_id)) {
        return res.status(404).send();
    }

    UserModel.findByIdAndDelete(user_id).then((doc) => {
        if (!doc) {
            return res.status(404).send();
        }
        res.status(200).send(doc._id);
    }).catch((e) => {
        res.status(400).send(e);
    });

});
//--- /manage -> /users/profiles

//--- /manage -> /users/vacation
app.get('/manage/users/vacation', authenticate, (req, res) => {
    VacationModel.aggregate([
        ...VacationAggregate
    ]).then((docs) => {
        res.status(200).send(docs);
    }).catch((e) => {
        res.status(404).send(e);
    })
});

app.get('/manage/users/vacation/search', authenticate, (req, res) => {
    let params = _.pick(req.query, ['status', 'inspectorNotes', 'requesterNotes']);
    let finder = {};
    if (params.status !== undefined && params.status !== "") finder.status = new RegExp('.*' + params.status + '.*', 'i');
    if (params.inspectorNotes !== undefined && params.inspectorNotes !== "") finder.inspectorNotes = new RegExp('.*' + params.inspectorNotes + '.*', 'i');
    if (params.requesterNotes !== undefined && params.requesterNotes !== "") finder.requesterNotes = new RegExp('.*' + params.requesterNotes + '.*', 'i');

    VacationModel.aggregate([
        { $match: finder },
        ...VacationAggregate
    ]).then(docs => {
        res.status(200).send(docs);
    }).catch((e) => {
        res.status(404).send(e);
    })
});

app.patch('/manage/users/vacation', authenticate, (req, res) => {
    let vacation_id = req.body.id;
    let body = _.pick(req.body, ['inspectorNotes', 'status', 'inspectorId']);

    if (!ObjectID.isValid(vacation_id)) {
        return res.status(404).send();
    }

    if (!(body.status && (body.status === "In attesa" || body.status === "Rifiutata" || body.status === "Approvata"))) {
        return res.status(400).send();
    }

    VacationModel.findByIdAndUpdate(vacation_id, { $set: body }, { new: true }).then((doc) => {
        VacationModel.aggregate([
            {
                $match: { _id: mongoose.Types.ObjectId(doc._id) }
            },
            ...VacationAggregate
        ]).then((aggregateDoc) => {
            res.status(200).send(aggregateDoc[0]);
        }).catch((e) => {
            res.status(404).send(e);
        });
    }).catch((e) => {
        res.status(400).send(e);//Problem when try to patch
    });

});
//--- /manage -> /users/vacation

//--- /manage -> /users/roles
app.get('/api/roles/', authenticate, (req, res) => {
    UserRoleModel.find().sort({ name: 1 }).then((userRoles) => {
        res.status(200).send(userRoles);
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.get('/api/roles/search', authenticate, (req, res) => {

    let params = _.pick(req.query, ['name']);
    let finder = {};
    if (params.name !== undefined && params.name !== "") finder.name = new RegExp('.*' + params.name + '.*', 'i');

    UserRoleModel.find(finder).sort({ name: 1 }).then((doc) => {
        res.status(200).send(doc);
    }).catch((e) => {
        res.status(404).send(e);
    });
});

app.post('/api/roles/', authenticate, (req, res) => {
    let body = _.pick(req.body, [
        "name",
        "roleType",
        "authorizationIds"
    ]);
    let newUserRole = new UserRoleModel(body);

    newUserRole.save().then((doc) => {
        res.status(200).send(doc);
    }).catch((e) => {
        res.status(400).send(e);
    });

});

app.put('/api/roles/', authenticate, (req, res) => {
    let role_id = req.body.id;
    let body = _.pick(req.body, [
        "name",
        "roleType",
        "authorizationIds"
    ]);

    if (!ObjectID.isValid(role_id)) {
        return res.status(404).send();
    }

    UserRoleModel.findByIdAndUpdate(role_id, { $set: body }, { new: true }).then((doc) => {
        if (!doc) {
            return res.status(404).send();
        }

        return res.status(200).send(doc);
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.delete('/api/roles/:id', (req, res) => {
    let role_id = req.params.id;

    if (!ObjectID.isValid(role_id)) {
        return res.status(404).send();
    }

    UserRoleModel.findByIdAndDelete(role_id).then((doc) => {
        if (!doc) {
            return res.status(404).send();//No Match for this id
        }
        return res.status(200).send(doc._id);

    }).catch((e) => {
        res.status(400).send(e);//Problem when try to delete
    });
});

//--- /manage -> /users/roles

//---- manage -> users/roles ->AuthorizationMap
app.get('/api/roles/authorizationmap', authenticate, (req, res) => {
    AuthorizationModel.find().then((docs) => {
        res.status(200).send(docs);
    }).catch((e) => {
        res.status(400).send(e);
    });
});

//---- manage -> users/roles ->AuthorizationMap

//--- /users/me/vacation
app.get('/users/me/vacation', authenticate, (req, res) => {

    VacationModel.aggregate([
        {
            $match: {
                requesterId: mongoose.Types.ObjectId(req.user._id)
            }
        },
        ...VacationAggregate
    ]).then((vacation) => {
        res.status(200).send(vacation);
    }).catch((e) => {
        res.status(404).send(e);
    })

});

app.get('/users/me/vacation/search', authenticate, (req, res) => {
    let params = _.pick(req.query, ['status', 'inspectorNotes', 'requesterNotes']);
    let finder = {};
    if (params.status !== undefined && params.status !== "") finder.status = new RegExp('.*' + params.status + '.*', 'i');
    if (params.inspectorNotes !== undefined && params.inspectorNotes !== "") finder.inspectorNotes = new RegExp('.*' + params.inspectorNotes + '.*', 'i');
    if (params.requesterNotes !== undefined && params.requesterNotes !== "") finder.requesterNotes = new RegExp('.*' + params.requesterNotes + '.*', 'i');

    VacationModel.aggregate([
        {
            $match: {
                requesterId: mongoose.Types.ObjectId(req.user._id)
            }
        },
        ...VacationAggregate
    ]).then(docs => {
        res.status(200).send(docs);
    }).catch((e) => {
        res.status(404).send(e);
    })
});

app.post('/users/me/vacation', authenticate, (req, res) => {
    let body = _.pick(req.body, [
        "startDate",
        "endDate",
        "requesterNotes"]);
    body.status = "In attesa";
    body.requesterId = req.user._id;

    newVacation = new VacationModel(body);

    newVacation.save().then((doc) => {
        VacationModel.aggregate([
            {
                $match: { _id: mongoose.Types.ObjectId(doc._id) }
            },
            ...VacationAggregate
        ]).then((aggregateDoc) => {
            res.status(200).send(aggregateDoc[0]);
        }).catch((e) => {
            res.status(404).send(e);
        });
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.patch('/users/me/vacation', authenticate, (req, res) => {
    let vacation_id = req.body.id;
    let body = _.pick(req.body, [
        "startDate",
        "endDate",
        "requesterNotes"]);

    if (!ObjectID.isValid(vacation_id)) {
        return res.status(404).send();//Invalid id
    }

    VacationModel.findByIdAndUpdate(vacation_id, { $set: body }, { new: true }).then((doc) => {
        VacationModel.aggregate([
            {
                $match: { _id: mongoose.Types.ObjectId(doc._id) }
            },
            ...VacationAggregate
        ]).then((aggregateDoc) => {
            res.status(200).send(aggregateDoc[0]);
        }).catch((e) => {
            res.status(404).send(e);
        });
    }).catch((e) => {
        res.status(400).send(e);//Problem when try to patch
    });

});

app.delete('/users/me/vacation/:id', authenticate, (req, res) => {
    let vacation_id = req.params.id;
    if (!ObjectID.isValid(vacation_id)) {
        return res.status(404).send();
    }

    VacationModel.findByIdAndDelete(vacation_id).then((doc) => {
        if (!doc) {
            return res.status(404).send();
        }
        res.status(200).send(doc._id);
    }).catch((e) => {
        res.status(400).send(e);//Problem when try to delete
    });
});
//--- /users/me/vacation

//--- users/me/profile
app.patch('/users/me/profile', authenticate, (req, res) => {
    let user_id = req.user._id;
    let body = _.pick(req.body, ['mail', 'phone', 'mobilePhone']);

    if (!ObjectID.isValid(user_id)) {
        return res.status(404).send();
    }

    UserModel.findByIdAndUpdate(user_id, { $set: body }, { new: true }).then((doc) => {
        if (!doc) {
            return res.status(404).send();
        }

        return res.status(200).send(doc);
    }).catch((e) => {
        res.status(400).send();
    });

});
//--- users/me/profile

//--- users authentication 
app.post('/api/login/authenticate', (req, res) => {
    let body = _.pick(req.body, ['username', 'password']);
    UserModel.findByCredentials(body.username, body.password).then((user_doc) => {
        if (!user_doc) {
            return res.status(404).send();
        }

        UserRoleModel.find({ _id: { $in: user_doc.roles } }).then((userRole_doc) => {

            if (!userRole_doc) {
                return res.status(404).send();
            }

            user_doc.generateAuthToken().then((token) => {
                // res.header('Authorization', token).send({ data: { user: { ...user_doc.toJSON(), roles: userRole_doc } } });
                res.header('Authorization', token).send({ user: { ...user_doc.toJSON(), roles: userRole_doc }, token: token });
            });
        }).catch((e) => {
            return (e);
        });
    }).catch((e) => {
        res.status(400).send(e);
    });
});

app.get('/api/users/current', authenticate, (req, res) => {
    let user_id = req.user._id;
    UserModel.findById(user_id).then((user_doc) => {
        if (!user_doc) {
            return res.status(404).send();
        }

        UserRoleModel.find({ _id: { $in: user_doc.roles } })
            .then((userRole_doc) => {

                if (!userRole_doc) {
                    return res.status(404).send();
                }


                res.status(200).send({ ...user_doc.toJSON(), roles: userRole_doc });
            }).catch((e) => {
                return (e);
            });
    }).catch((e) => {
        res.status(400).send();
    });
});


app.delete('/users/me/token', authenticate, (req, res) => {
    req.user.removeToken(req.token).then(() => {
        res.status(200).send({ msg: "User Token Deleted" });
    }, (e) => {
        res.status(400).send(e);
    });
});
//--- users authentication 



module.exports = { app };
