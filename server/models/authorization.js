const mongoose = require('mongoose');
const _ = require('lodash');

const Schema = mongoose.Schema;

var AuthorizationsSchema = new Schema({
    _id: { type: String, required: true, unique: true },
    enabled: { type: Boolean },
    description: { type: String, required: true },
}, { _id: false })

var AuthorizationSchema = new Schema({
    roleType: { type: Number, required: true },
    authorizations: [
        AuthorizationsSchema
    ]
});

AuthorizationsSchema.methods.toJSON = function () {
    var authorizations = this;
    var authorizationsObject = authorizations.toObject();
    let output = _.pick(authorizationsObject, ['enabled', 'description']);
    output.id = authorizationsObject['_id'];
    return output;
}

var AuthorizationModel = mongoose.model('authorization', AuthorizationSchema);


module.exports = { AuthorizationModel };