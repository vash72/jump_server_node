const mongoose = require('mongoose');
const _ = require('lodash');
var Schema = mongoose.Schema;

var OfficeSchema = new Schema({
    name: { type: String, required: true }
});

OfficeSchema.methods.toJSON = function () {
    var office = this;
    var officeObject = office.toObject();
    let output = _.pick(officeObject, ['name']);
    output.id = officeObject._id;
    return output;
}

var OfficeModel = mongoose.model('office', OfficeSchema);

module.exports = { OfficeModel };