const mongoose = require('mongoose');
const _ = require('lodash');
var Schema = mongoose.Schema;

var AreaSchema = new Schema({
    name: { type: String, required: true },
    office: { type: Schema.Types.ObjectId, ref: 'office', required: true },
    department: { type: Schema.Types.ObjectId, ref: 'department', required: true }
});

AreaSchema.methods.toJSON = function () {
    var area = this;
    var areaObject = area.toObject();
    let output = _.pick(areaObject, ['name','office','department']);
    output.id = areaObject._id;
    return output;
}

var AreaModel = mongoose.model('area', AreaSchema);

module.exports = { AreaModel };